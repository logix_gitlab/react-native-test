import React, {Component} from 'react';
import {View ,FlatList ,Text ,StyleSheet ,Image ,TouchableOpacity} from 'react-native';
import { Card } from 'react-native-elements';

//import firebase from 'firebase';

export default class Client_detail extends Component{
    constructor(props){      
        super(props)
        this.state ={
          isLoading: true,
          user_id:"",
          provider_id:"",
          dataSource:"",
          job_desc:false
        }
        this.show_desc = this.show_desc.bind(this);
    }
    show_desc(description){
        alert(description);
    }
    componentDidMount(){
        return fetch('http://demo.logixbuilt.com/client.json')
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      this.setState({dataSource:responseJson})
    })
    .catch((error) => {
      console.error(error);
    });
    }
    render(){
        return (
        <View style={styles.container}>
            <FlatList
                data={this.state.dataSource}
                renderItem={({item}) =><View>
                    <TouchableOpacity onPress={() => {this.show_desc(item.description)}}>
                        <Card title={item.name}>
                            <View style={styles.card_view}>
                                <Image
                                    style={{width:50,height:50}}
                                    source={require('../uploads/favicon.png')}
                                />
                                <Text style={styles.text}>{item.job_title}</Text>                                
                            </View>
                        </Card>                   
                    </TouchableOpacity>
                </View>}
                keyExtractor={({name}, index) => name}
            />            
        </View>
        );
    }
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    padding:5,
  },
  card_view:{
      flexDirection:"row"
  },
  text:{
      paddingLeft:10,
  }
});

